import React, { useState } from "react";
import Layout from "../components/Layout/Layout";
import Typography from "@mui/material/Typography";
import SearchBar from "../components/Layout/SearchBar";
import ItemsGrid from "../components/Layout/ItemsGrid";
import Loading from "../components/Layout/Loading";
import { Container, Paper } from "@mui/material";
import HeroBannerImage from "../images/layoutImage.jpg"; // replace with your image path

const Home = () => {
  const [searchResults, setSearchResults] = useState(null);
  const [dateFrom, setDateFrom] = useState(null);
  const [dateTo, setDateTo] = useState(null);

  return (
    <Layout>
      <Paper
        sx={{
          backgroundImage: `url(${HeroBannerImage})`,
          backgroundSize: "cover",

          mb: 4,
          p: 4,
          textAlign: "center",
        }}
      >
        <Typography variant="h2" gutterBottom>
          Welcome to Our Hotel
        </Typography>
        <Typography variant="h5">
          Experience comfort and luxury like never before
        </Typography>
      </Paper>

      <Container>
        <SearchBar
          setSearchResults={setSearchResults}
          setDateFromProp={setDateFrom}
          setDateToProp={setDateTo}
        />

        {!searchResults ? (
          <Typography variant="h6" textAlign="center" sx={{ my: 4 }}>
            Enter your dates to see available rooms
          </Typography>
        ) : searchResults.length === 0 ? (
          <Typography textAlign="center">No available rooms</Typography>
        ) : (
          <ItemsGrid
            data={searchResults}
            dateFrom={dateFrom}
            dateTo={dateTo}
            sx={{ mt: 2 }}
          />
        )}
      </Container>
    </Layout>
  );
};

export default Home;
