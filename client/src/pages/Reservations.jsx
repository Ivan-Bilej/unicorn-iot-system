import React, { useEffect, useState } from "react";
import Layout from "../components/Layout/Layout";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import axiosInstance from "../axios";
import ItemsGrid from "../components/Layout/ItemsGrid";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import useUser from "../hooks/useUser";
import Loading from "../components/Layout/Loading";

export default function Reservations() {
  const [reservationsData, setReservationsData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const userData = useUser();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      if (userData && userData.data._id) {
        const response = await axiosInstance.get(
          `/reservation/user/${userData.data._id}`
        );
        setReservationsData(response.data);
        setIsLoading(false);
      }
    };

    fetchData();
  }, [userData]);

  const handleReturnHome = () => {
    navigate("/");
  };

  return (
    <Layout>
      <Loading isLoading={isLoading} />
      {reservationsData.length > 0 ? (
        <ItemsGrid data={reservationsData} sx={{ mt: 2 }} />
      ) : (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center", // Center horizontally
            alignItems: "center", // Center vertically
            height: "100%", // Take up full container height
          }}
        >
          <div>
            <Typography variant="h6" textAlign="center">
              You dont have any reservation yet.
            </Typography>
            <Button onClick={() => handleReturnHome()}>Create one!</Button>
          </div>
        </Box>
      )}
    </Layout>
  );
}
