import Layout from "../components/Layout/Layout";
import { Button, Card, Container, Grid } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import AddIcon from "@mui/icons-material/Add";
import RoomCard from "../components/Layout/RoomCard";
import RoomCardModal from "../components/Layout/RoomCardModal.jsx";
import { useState, useEffect } from "react";
import EditRoomModal from "../components/Layout/EditRoomModal.jsx";
import * as React from "react";
import DeleteRoomModal from "../components/Layout/DeleteConfirmationModal.jsx";
import CreateRoomModal from "../components/Layout/CreateRoomModal.jsx";
import axiosInstance from "../axios.js";
import RoomModal from "../components/Layout/RoomModal.jsx";
import Loading from "../components/Layout/Loading.jsx";
export default function Admin() {
  const [isLoading, setIsLoading] = useState(true);
  const fetchData = async () => {
    const roomList = await axiosInstance.get("/room/list");
    const roomTypes = await axiosInstance.get("/roomTypes");
    setRooms(roomList.data);
    setRoomTypes(roomTypes.data);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const [rooms, setRooms] = useState([]);
  const [roomTypes, setRoomTypes] = useState([]);

  // const handleEdit = (index) => {
  //     console.log(rooms[index])
  // }

  // const [open, setOpen] = useState(false);
  //
  // const handleOpen = () => setOpen(true);
  // const handleClose = () => setOpen(false);
  //
  // const [selectedEditButtonIndex,setSelectedEditButtonIndex] = useState(null)

  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [openCreate, setOpenCreate] = useState(false);
  const [editedRoom, setEditedRoom] = useState(null);
  const [deleteSelectedRoomIndex, setDeleteSelectedRoomIndex] = useState(null);

  const handleOpenEdit = (roomIndex) => {
    setEditedRoom(rooms[roomIndex]);
    setOpenEdit(true);
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
    fetchData();
  };
  const handleOpenDelete = (roomIndex) => {
    setDeleteSelectedRoomIndex(roomIndex);
    setOpenDelete(true);
  };

  const handleCloseDelete = () => {
    setDeleteSelectedRoomIndex(null);
    setOpenDelete(false);
  };
  const handleOpenCreate = () => {
    setOpenCreate(true);
  };

  const handleCloseCreate = () => {
    setOpenCreate(false);
  };

  const handleDeleteRoom = () => {
    console.log("deleted room", rooms[deleteSelectedRoomIndex]);
    const updatedRooms = [...rooms];
    updatedRooms.splice(deleteSelectedRoomIndex, 1);
    setRooms(updatedRooms);
  };
  // const handleRemoveItem = (index) => {
  //     const updatedItems = [...items];
  //     updatedItems.splice(index, 1);
  //     setItems(updatedItems);
  // };

  // const handleEditModal = () => {
  //     // Handle your edit logic here
  //
  //     // Close the modal
  //     handleCloseEdit();
  // };

  // const handleDeleteModal = () => {
  //     // Handle your edit logic here
  //
  //     // Close the modal
  //     handleCloseDelete();
  // };

  // const handleEditModal = () => {}
  // Handle your edit logic here

  // Close the modal
  // handleClose();
  // const handleEditModal = (editButtonIndex) =>{
  //     handleOpen()
  //     setSelectedEditButtonIndex(editButtonIndex)
  // }

  // const buttons = [
  //     // <Button color="info" variant="contained" startIcon={<EditIcon/>} aria-label={"Edit"} onClick={() => handleEditModal(roomIndex)}>Edit</Button>
  //     {
  //         color: "info",
  //         variant: "contained",
  //     }
  //     // <EditRoomModal aria-label={"OpenModal"}></EditRoomModal>,
  //     // <Button color="info" variant="contained" startIcon={<EditIcon/>} aria-label={"Edit"}>Edit</Button>,
  //     // <Button color="error" variant="contained" startIcon={<DeleteIcon />} aria-label={"Delete"}>Delete</Button>
  // ]
  // const buttons: [
  //     { aria-label="Button 1", onClick: () => console.log('Button 1 clicked') },
  //     { aria-label="Button 2", onClick: () => console.log('Button 2 clicked') },
  // //
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <Layout>
      <Loading isLoading={isLoading} />
      <Container>
        <Grid container spacing={4}>
          {rooms.map((room, roomIndex) => (
            <RoomCard
              roomName={room.type.name}
              capacity={room.type.capacity}
              description={room.type.description}
              price={room.type.price}
              roomId={room._id}
              key={room._id}
              roomNumber={room.number}
              roomIndex={roomIndex}
              buttons={[
                {
                  label: "Edit",
                  color: "info",
                  variant: "contained",
                  startIcon: <EditIcon />,
                  onClick: handleOpenEdit,
                },
                {
                  label: "Delete",
                  color: "error",
                  variant: "contained",
                  startIcon: <DeleteIcon />,
                  onClick: handleOpenDelete,
                },
                // Add more buttons as needed
              ]}
            />
          ))}

          <Grid sx={{ padding: 2 }}>
            <Card style={{ height: 250, width: 345 }}>
              <Button
                fullWidth
                color={"success"}
                variant={"contained"}
                sx={{ height: "100%" }}
                onClick={handleOpenCreate}
              >
                <AddIcon fontSize={"large"} />
              </Button>
            </Card>
          </Grid>
          {/*openEdit:{openEdit}*/}
          {/*openDelete:{openDelete}*/}

          {/*<Button color="info" variant="contained" startIcon={<EditIcon/>} aria-label={"Edit"} onClick={() => handleEditModal(roomIndex)}>Edit</Button>*/}
          {/* <EditRoomModal
            open={openEdit}
            handleClose={handleCloseEdit}
            room={
              editSelectedRoomIndex !== null ? rooms[editSelectedRoomIndex] : {}
            }
            // key={editSelectedRoomIndex !== null ? editSelectedRoomIndex : ''}
            roomIndex={
              editSelectedRoomIndex !== null ? editSelectedRoomIndex : ""
            }
            // handleEditModal={handleEditModal}
          /> */}

          <DeleteRoomModal
            open={openDelete}
            handleClose={handleCloseDelete}
            handleDelete={handleDeleteRoom}
            confirmationText={"Are you sure you want to delete room: "}
            room={
              deleteSelectedRoomIndex !== null
                ? rooms[deleteSelectedRoomIndex]
                : {}
            }
            // key={deleteSelectedRoomIndex !== null ? deleteSelectedRoomIndex : ''}
            roomIndex={
              deleteSelectedRoomIndex !== null ? deleteSelectedRoomIndex : ""
            }
            // Any other props you need to pass
          />
          {/*<CreateRoomModal open={handleOpenCreate} handleClose={handleCloseCreate}/>*/}

          {/*<RoomCardModal openState={open}/>*/}
          <div>
            {/*<EditRoomModal setOpen>*/}
            {/*</EditRoomModal>*/}

            {/*<Button onClick={handleOpen}>Open modal</Button>Button*/}
            {/*{open ?*/}
            {/*    <Modal*/}
            {/*        open={open}*/}
            {/*        onClose={handleClose}*/}
            {/*        aria-labelledby="modal-modal-title"*/}
            {/*        aria-describedby="modal-modal-description"*/}
            {/*    >*/}
            {/*        <Box sx={style}>*/}
            {/*            <Typography id="modal-modal-title" variant="h6" component="h2">*/}

            {/*            </Typography>*/}
            {/*            <Typography id="modal-modal-description" sx={{mt: 2}}>*/}
            {/*                Duis mollis, est non commodo luctus, nisi erat porttitor ligula.*/}
            {/*                /!*{rooms._id.find(selectedEditButtonIndex)}.*!/*/}
            {/*                {selectedEditButtonIndex}*/}
            {/*            </Typography>*/}
            {/*        </Box>*/}
            {/*    </Modal>*/}
            {/*    : <div></div>}*/}
          </div>
        </Grid>
        <RoomModal
          open={openEdit}
          onClose={handleCloseEdit}
          roomTypes={roomTypes}
          editedRoom={editedRoom}
        />
        <CreateRoomModal
          open={openCreate}
          roomTypes={roomTypes}
          onClose={handleCloseCreate}
        />
      </Container>
    </Layout>
  );
}
