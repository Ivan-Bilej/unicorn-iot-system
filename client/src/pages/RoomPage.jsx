import React, { useEffect, useState  } from "react";
import { useParams } from "react-router-dom";
import Layout from "../components/Layout/Layout";
import Typography from "@mui/material/Typography";
import RoomDetail from "../components/Layout/RoomDetail";
import axiosInstance from '../axios';


export default function RoomPage() {  
  const { id } = useParams();
  const [roomData, setRoomData] = useState(null);

  useEffect(() => {
    const fetchRoomData = async () => {
      const response = await axiosInstance.get(`/room/${id}`);
      setRoomData(response.data);
    };

    fetchRoomData();
  }, [id]);

  return(
    <Layout>
      <Typography variant="h4" component="h1" gutterBottom>
          Room Page
      </Typography>

      <RoomDetail roomData={roomData}></RoomDetail>
    </Layout>
  );
}