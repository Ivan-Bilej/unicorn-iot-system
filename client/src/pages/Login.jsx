import React, { useState } from "react";
import { Button } from "@mui/material";
import GoogleIcon from "@mui/icons-material/Google";
import axiosInstance from "../axios";
export default function Login() {
  const handleGoogleLogin = async () => {
    await axiosInstance.get("/user/loginLink").then((res) => {
      window.location.href = res.data.loginLink;
    });
    // Redirect to your backend Google login route
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <Button
        variant="contained"
        startIcon={<GoogleIcon />}
        onClick={handleGoogleLogin}
      >
        Sign in with Google
      </Button>
    </div>
  );
}
