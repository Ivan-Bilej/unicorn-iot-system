import React, { useEffect, useState  } from "react";
import { useParams, useSearchParams  } from "react-router-dom";
import Layout from "../components/Layout/Layout";
import Typography from "@mui/material/Typography";
import RoomDetail from "../components/Layout/RoomDetail";
import axiosInstance from '../axios';


export default function Room() {  
  const { roomId } = useParams();
  const [roomData, setRoomData] = useState(null);
  const [searchParams] = useSearchParams()
  const dateFrom = new Date(searchParams.get("dateFrom"))
  const dateTo = new Date(searchParams.get("dateTo"))
  const reservationId = searchParams.get("reservationId")

  useEffect(() => {
    const fetchRoomData = async () => {
      const response = await axiosInstance.get(`/room/${roomId}`);
      setRoomData(response.data);
    };

    fetchRoomData();
  }, [roomId]);

  return(
    <Layout>
      <Typography variant="h4" component="h1" gutterBottom>
          Room Page
      </Typography>

      <RoomDetail 
        roomData={roomData}
        dateFromProp={dateFrom}
        dateToProp={dateTo}
        reservationIdProp={reservationId}
      ></RoomDetail>
    </Layout>
  );
}