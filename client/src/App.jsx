import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from "./pages/Home";
import Error from "./pages/Error";
import Login from "./pages/Login";
import Admin from "./pages/Admin";

import Room from "./pages/Room";
import Reservations from "./pages/Reservations";
import ProtectedRoute from "./components/auth/ProtectedRoute";
import PublicRoute from "./components/auth/PublicRoute";
import "./App.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <ProtectedRoute>
        <Home />
      </ProtectedRoute>
    ),
    errorElement: <Error />,
  },
  {
    path: "/login",
    element: (
      <PublicRoute>
        <Login />
      </PublicRoute>
    ),
    errorElement: <Error />,
  },
  {
    path: "/roomDetail/:roomId",
    element: (
      <ProtectedRoute>
        <Room />
      </ProtectedRoute>
    ),
    errorElement: <Error />,
  },
  {
    path: "/admin",
    element: (
      <ProtectedRoute>
        <Admin />
      </ProtectedRoute>
    ),
  },
  {
    path: "/reservations",
    element: (
      <ProtectedRoute>
        <Reservations />
      </ProtectedRoute>
    ),
    errorElement: <Error />,
  },
]);

// PublicRoute checks if user is authenticated and redirects them away from login if they are

function App() {
  return <RouterProvider router={router} />;
}

export default App;
