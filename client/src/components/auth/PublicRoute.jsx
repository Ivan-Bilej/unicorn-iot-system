import React from "react";
import useAuth from "../../hooks/useAuth"; // Import the useAuth hook
import { Navigate } from "react-router-dom";
const PublicRoute = ({ children }) => {
  const isAuthenticated = useAuth();

  if (isAuthenticated === null) {
    // Auth check is in progress
    return <div>Loading...</div>;
  }

  if (isAuthenticated) {
    // Redirect authenticated users to the homepage
    return <Navigate to="/" replace />;
  }

  return children;
};

export default PublicRoute;
