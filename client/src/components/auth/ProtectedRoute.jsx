import React from "react";
import { Navigate } from "react-router-dom";
import useAuth from "../../hooks/useAuth"; // Import the useAuth hook
const ProtectedRoute = ({ children }) => {
  const isAuthenticated = useAuth();

  if (isAuthenticated === null) {
    // Maybe show a loading spinner while the auth check is happening
    return <div>Loading...</div>;
  }

  return isAuthenticated ? children : <Navigate to="/login" />;
};

export default ProtectedRoute;
