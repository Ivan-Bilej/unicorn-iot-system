import React from "react";

import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Container,
  Grid,
  Typography,
} from "@mui/material";
const RoomCard = ({
  roomName,
  capacity,
  description,
  price,
  buttons,
  roomNumber,
  roomIndex,
}) => {
  return (
    <Grid sx={{ padding: 2 }}>
      <Card style={{ height: 250, width: 345 }}>
        <CardHeader title={roomName} />
        <CardContent>
          <Typography noWrap>Room number: {roomNumber}</Typography>
          <Typography noWrap>Capacity: {capacity}</Typography>
          <Typography noWrap>Description {description}</Typography>
          <Typography noWrap>Price: {price}</Typography>
        </CardContent>
        <CardActions>
          {buttons.map((button, buttonIndex) => (
            <Button
              key={buttonIndex}
              color={button.color}
              variant={button.variant}
              startIcon={button.startIcon}
              aria-label={button.label}
              onClick={() => button.onClick(roomIndex)} // Passing roomName to onClick
            >
              {button.label}
            </Button>
          ))}
        </CardActions>
      </Card>
    </Grid>
  );
};

export default RoomCard;
