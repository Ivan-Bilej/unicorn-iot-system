import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import EditIcon from "@mui/icons-material/Edit";
import { InputAdornment, OutlinedInput, Stack, TextField } from "@mui/material";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import { useState } from "react";

// Save all changes in variables and then on submit, submit updatedRoom to the corresponging ID in the actual room Data array
//Then on submit, create a new object and pass it that way
//to get roomtypes => axios api get room types
const EditRoomModal = ({ open, handleClose, room, key, roomIndex }) => {
  const { roomName, capacity, description, price, roomId } = room;

  const [updatedRoomName, setUpdatedRoomName] = useState(room.roomName);

  const handleChangeRoomName = (e) => {
    // console.log("here a change:",e)
    setUpdatedRoomName(e);
    console.log("updatedRoom:", updatedRoomName);
  };

  const submitChanges = () => {
    console.log(updatedRoomName);
  };

  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };
  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Update Room Info
            {updatedRoomName}
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <Stack
              direction="column"
              justifyContent="flex-start"
              alignItems="flex-start"
              spacing={2.5}
            >
              <TextField
                id="outlined-basic"
                label="Room Name"
                variant="outlined"
                defaultValue={roomName}
                onChange={(e) => handleChangeRoomName(e.target.value)}
                fullWidth
              />
              <FormControl fullWidth>
                <InputLabel id="capacity">Capacity</InputLabel>
                <Select
                  labelId="capacity-label"
                  id="capacity-select"
                  value={capacity}
                  label="Capacity"
                >
                  <MenuItem value={1}>1</MenuItem>
                  <MenuItem value={2}>2</MenuItem>
                  <MenuItem value={3}>3</MenuItem>
                  <MenuItem value={4}>4</MenuItem>
                </Select>
              </FormControl>
              <TextField
                labelId="description-label"
                id="description"
                label="Description"
                multiline
                maxRows={4}
                fullWidth
                defaultValue={description}
              />

              <FormControl fullWidth>
                <InputLabel htmlFor="outlined-adornment-amount">
                  Amount
                </InputLabel>
                <OutlinedInput
                  min={0}
                  type="number"
                  id="outlined-adornment-amount"
                  startAdornment={
                    <InputAdornment position="start">€</InputAdornment>
                  }
                  label="Amount"
                />
              </FormControl>
              <Stack
                direction="row"
                justifyContent="flex-start"
                alignItems="flex-end"
                spacing={1}
              >
                <Button onClick={handleClose} variant="contained" color="info">
                  Cancel
                </Button>
                <Button
                  variant="contained"
                  color="success"
                  onClick={submitChanges}
                >
                  Save Changes
                </Button>
              </Stack>
            </Stack>
          </Typography>
        </Box>
      </Modal>
    </div>
  );
};

export default EditRoomModal;
