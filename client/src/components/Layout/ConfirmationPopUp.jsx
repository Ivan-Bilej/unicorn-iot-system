import React, { useEffect } from 'react';
import DialogContentText from '@mui/material/DialogContentText';
import {Alert, Snackbar} from "@mui/material";

const ConfirmationPopUp = ({ open, onClose, title, contentLines, autoCloseDelay = 3000 }) => {
  useEffect(() => {
    if (open) {
      const timer = setTimeout(onClose, autoCloseDelay);
      return () => clearTimeout(timer);
    }
  }, [open, onClose, autoCloseDelay]);

  return (
      <Snackbar open={open} autoHideDuration={6000} onClose={onClose}>
      <Alert
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description">
        {contentLines?
            (contentLines.map((line, index) => (
                <DialogContentText key={index}>
                {line}
            </DialogContentText>
            )))
            :null
        }
    </Alert>
      </Snackbar>
  );
};
export default ConfirmationPopUp;
