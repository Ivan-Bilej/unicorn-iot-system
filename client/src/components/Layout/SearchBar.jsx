import React, { useEffect, useState } from "react";
import dayjs from "dayjs";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import {Box, Button, Stack} from "@mui/material";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import axiosInstance from "../../axios";
import Loading from "./Loading";

function getTodaysDate() {
  function padTo2Digits(num) {
    return num.toString().padStart(2, "0");
  }

  function formatDate(date) {
    return [
      padTo2Digits(date.getMonth() + 1),
      padTo2Digits(date.getDate()),
      date.getFullYear(),
    ].join("/");
  }

  return formatDate(new Date());
}

export default function SearchBar({
  setSearchResults,
  setDateFromProp,
  setDateToProp,
}) {
  const [dateFrom, setDateFrom] = useState(dayjs(getTodaysDate()));
  const [dateTo, setDateTo] = useState(dayjs(getTodaysDate()));
  const [isLoading, setIsLoading] = useState(false);
  const handleSearch = () => {
    setIsLoading(true);
    setDateFromProp(dateFrom);
    setDateToProp(dateTo);

    axiosInstance
      .get(`/room/available-rooms?dateFrom=${dateFrom}&dateTo=${dateTo}`)
      .then((res) => {
        setSearchResults(res.data);
        setIsLoading(false);
      });
  };

  return (
    <div>
      <Loading isLoading={isLoading} />
      <Box
        component="form"
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          m: 2,
          "@media (max-width: 650px)": {
            flexDirection: "column",
            alignItems: "center",
          },
        }}
      >
          <Stack
                  direction={{ xs: 'column', sm: 'row' }}
                  spacing={{ xs: 1, sm: 2, md: 4 }}
              >
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DatePicker
              value={dateFrom}
              label="Date From"
              onChange={(newDateFrom) => setDateFrom(newDateFrom)}
            />

            <DatePicker
              value={dateTo}
              label="Date To"
              onChange={(newDateTo) => setDateTo(newDateTo)}
            />
        </LocalizationProvider>

          <Button variant="outlined" onClick={handleSearch}>
            Search
          </Button>
          </Stack>
      </Box>
    </div>
  );
}
