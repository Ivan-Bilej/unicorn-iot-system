import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import { Box, Button, ImageList, ImageListItem,
  Paper, useTheme, useMediaQuery, CardMedia } from "@mui/material";
import { useNavigate } from "react-router-dom";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import Typography from "@mui/material/Typography";
import axiosInstance from "../../axios";
import ConfirmationPopUp from "./ConfirmationPopUp";
import useUser from "../../hooks/useUser";
import picture from "../../images/StandardRoom/foto2.jpg"


const fallbackImages = [
  "/path/to/image1.jpg",
  "/path/to/image2.jpg",
  "/path/to/image3.jpg",
  "/path/to/image4.jpg",
];


function padTo2Digits(num) {
  return num.toString().padStart(2, "0");
}

function formatDate(date) {
  return [
    padTo2Digits(date.getMonth() + 1),
    padTo2Digits(date.getDate()),
    date.getFullYear(),
  ].join("/");
}


export default function RoomDetail({
  roomData, reservationIdProp,
  dateFromProp,dateToProp, }) 
  {
  // Sample state for active image
  const [activeImage, setActiveImage] = useState(0);
  const [reservationId, setReservationId] = useState(reservationIdProp);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [confirmationText, setConfirmationText] = useState("")
  const [showError, setShowError] = useState(false);
  const [errorText, setErrorText] = useState("")
  const navigate = useNavigate();
  const userData = useUser();
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("md"));
  const isBiggerScreen = useMediaQuery(theme.breakpoints.up("lg"));

  const images =
    roomData && roomData.type
      ? [
        `/src/images/${roomData.type.name.replace(/\s/g, '')}/foto1.jpg`,
        `/src/images/${roomData.type.name.replace(/\s/g, '')}/foto2.jpg`
      ]
      : fallbackImages;
  


  // Handlers to navigate through images
  const handleNext = () => setActiveImage((prev) => (prev + 1) % images.length);
  const handlePrevious = () =>
    setActiveImage((prev) => (prev - 1 + images.length) % images.length);

  // Handlers for reservation
  const handleCreateReservation = async () => {
    const reservationData = {
      roomId: roomData._id,
      userId: userData.data._id,
      dateFrom: dateFromProp,
      dateTo: dateToProp,
    };

    try {
      const response = await axiosInstance.post(
        `/reservation/create`,
        reservationData
      );
      setReservationId(response.data._id);
      navigate(`/roomDetail/${roomData._id}?dateFrom=${dateFromProp}&dateTo=${dateToProp}&reservationId=${response.data._id}`)
      setShowConfirmation(true)
      setConfirmationText([
        `Reservation created successfully for room: ${roomData.number}`,
        `from: ${formatDate(dateFromProp)}`,
        `to: ${formatDate(dateToProp)}`
      ]);
    } catch (error) {
      setShowError(true)
      setErrorText(["Error creating reservation: ", error.response])
    }
  };
  const handleCancelReservation = async () => {
    await axiosInstance.delete(`/reservation/${reservationId}`);
    setReservationId(null);
    navigate(`/roomDetail/${roomData._id}?dateFrom=${dateFromProp}&dateTo=${dateToProp}`)
    //navigate(`/reservations`)
  };

  return (
    <Box sx={{ display: "flex", flexDirection: "column", p: 4 }}>
      <Grid container spacing={2} direction={isSmallScreen ? "column" : "row"}>
        <Grid item xs={9}>
          <Box
            sx={{ display: "flex", alignItems: "center", position: "relative" }}
          >
            <Button onClick={handlePrevious}>
              <ArrowBackIosNewIcon />
            </Button>

            <img
              src={images[activeImage]}
              alt="Main room"
              style={{ 
                width: "100%", 
                maxWidth: isSmallScreen? "250px" : isBiggerScreen? "600px" :"480px", 
                minHeight: "300px", 
                objectFit: "cover" }}
            />

            <Button onClick={handleNext}>
              <ArrowForwardIosIcon />
            </Button>
          </Box>

          <ImageList sx={{ mt: 2 }} cols={4}>
            {images.map((src, index) => ( 
            <ImageListItem key={index}>
                <img
                  src={src}
                  alt={`Room image ${index}`}
                  loading="lazy"
                  style={{ cursor: "pointer" }}
                  onClick={() => setActiveImage(index)}
                />
              </ImageListItem>)
             
            )}
          </ImageList>
        </Grid>

        <Grid item xs={3}>
          <Typography variant="h6">Date from</Typography>
          <Paper elevation={3} sx={{ p: 1.5 }}>
            <Typography variant="">
              {dateFromProp ? formatDate(dateFromProp) : null}
            </Typography>
          </Paper>

          <Typography variant="h6">Date to</Typography>
          <Paper elevation={3} sx={{ mb: 2, p: 1.5 }}>
            <Typography variant="">
              {dateToProp ? formatDate(dateToProp) : null}
            </Typography>
          </Paper>

          <Typography variant="h6">Room type</Typography>
          <Paper elevation={3} sx={{ p: 1.5 }}>
            <Typography variant="">
              {roomData ? roomData.type.name : null}
            </Typography>
          </Paper>

          <Typography variant="h6">Price</Typography>
          <Paper elevation={3} sx={{ mb: 2, p: 1.5 }}>
            <Typography variant="">
              {roomData ? roomData.type.price : null}
            </Typography>
          </Paper>

          {reservationId ? (
            <Button
              variant="contained"
              color="primary"
              fullWidth
              sx={{ mt: 2 }}
              onClick={handleCancelReservation}
            >
              Cancel reservation
              
            </Button>
      
          ) : (
            <Button
              variant="contained"
              color="primary"
              fullWidth
              sx={{ mt: 2 }}
              onClick={handleCreateReservation}
            >
              Reserve
            </Button>
          )}

        </Grid>

        <Grid item xs={12}>
          <Typography variant="h6" sx={{ mt: 2 }}>
            Description
          </Typography>
          <Paper elevation={3} sx={{ p: 2 }}>
            <Typography
              variant="body1"
              gutterBottom
              sx={{
                whiteSpace: "pre-wrap", // Allows text to wrap and preserves whitespace
                wordBreak: "break-word", // Breaks long words to prevent overflow
                overflowWrap: "break-word", // Allows breaking and wrapping long words
              }}
            >
              {roomData ? roomData.type.description : null}
            </Typography>
          </Paper>
        </Grid>
      </Grid>

      <ConfirmationPopUp
        open={showConfirmation}
        onClose={() => setShowConfirmation(false)}
        title="Success"
        contentLines={confirmationText}
      />
      <ConfirmationPopUp
        open={showError}
        onClose={() => setShowError(false)}
        title="Error"
        contentLines={errorText}
      />
    </Box>
  );
}
