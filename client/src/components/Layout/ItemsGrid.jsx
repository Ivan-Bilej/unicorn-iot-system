import * as React from "react";
import { styled } from "@mui/material/styles";
import { useNavigate } from "react-router-dom";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import {
  Typography,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  Stack,
  Divider,
  Icon,
} from "@mui/material";
import BedIcon from "@mui/icons-material/Bed";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

function padTo2Digits(num) {
  return num.toString().padStart(2, "0");
}

function formatDate(isoDate) {
  let date = new Date(isoDate);
  return [
    padTo2Digits(date.getDate()),
    padTo2Digits(date.getMonth() + 1),
    date.getFullYear(),
  ].join(".");
}

export default function ItemsGrid({ data, dateFrom, dateTo }) {
  const navigate = useNavigate();

  const handleOnClick = (roomId, dateFromProp, dateToProp, reservationId) => {
    if (reservationId) {
      navigate(
        `/roomDetail/${roomId}?dateFrom=${dateFromProp}&dateTo=${dateToProp}&reservationId=${reservationId}`
      );
    } else {
      navigate(
        `/roomDetail/${roomId}?dateFrom=${dateFromProp}&dateTo=${dateToProp}`
      );
    }
  };

  return (
    <Grid
      container
      sx={{ flexGrow: 1, padding: 2 }}
      spacing={{ xs: 2, md: 3 }}
      columns={{ xs: 2, sm: 8, md: 12 }}
    >
      {data.map((item) => {
        if (item.roomId) {
          const imagePath = `src/images/Thumbnails/${item.roomId.type.name.replace(/\s/g, "")}.jpg`

          return (
            <Grid
              item
              xs={2}
              sm={4}
              md={4}
              key={item._id}
              onClick={() =>
                handleOnClick(
                  item.roomId._id,
                  item.dateFrom,
                  item.dateTo,
                  item._id
                )
              }
            >
              <Card style={{ width: "100%" }}>
                <CardMedia
                  sx={{ height: 140 }}
                  image={imagePath}
                  title={item.roomId.type.name}
                />
                <CardHeader title={`Room number ${item.roomId.number}`} />
                <CardContent>
                  <Typography noWrap fontSize={25}>
                    <BedIcon fontSize="large" sx={{ mb: -1 }} />{" "}
                    {item.roomId.type.capacity}
                  </Typography>
                  <Typography noWrap>{item.roomId.type.description}</Typography>
                </CardContent>

                <Stack
                  direction="row"
                  justifyContent="space-evenly"
                  alignItems="center"
                  spacing={2}
                  padding={1.5}
                  divider={<Divider orientation="vertical" flexItem />}
                >
                  <Typography alt={"Date From"} title="Date From">
                    {formatDate(item.dateFrom)}
                  </Typography>
                  <Typography alt={"Date To"} title="Date To">
                    {formatDate(item.dateTo)}
                  </Typography>
                </Stack>

                <Stack
                  direction="row"
                  justifyContent="space-evenly"
                  alignItems="center"
                  spacing={2}
                  padding={1.5}
                  divider={<Divider orientation="vertical" flexItem />}
                >
                  <Typography alt={"Room Type"} title="Room Type">
                    {item.roomId.type.name}
                  </Typography>
                  <Typography alt={"Price"} title="Price">
                    {item.roomId.type.price + ",-Kč"}
                  </Typography>
                </Stack>
              </Card>
            </Grid>
          );
        } else {
          const imagePath = `src/images/Thumbnails/${item.type.name.replace(/\s/g, "")}.jpg`;

          return (
            <Grid
              item
              xs={2}
              sm={4}
              md={4}
              key={item._id}
              onClick={() => handleOnClick(item._id, dateFrom, dateTo)}
            >
              <Card style={{ width: "100%" }}>
                <CardMedia
                  sx={{ height: 140 }}
                  image={imagePath}
                  title={item.type.name}
                />
                <CardHeader title={`Room number ${item.number}`} />
                <CardContent>
                  <Typography noWrap fontSize={25}>
                    <BedIcon fontSize="large" sx={{ mb: -1 }} />{" "}
                    {item.type.capacity}
                  </Typography>
                  <Typography noWrap>{item.type.description}</Typography>
                </CardContent>

                <Stack
                  direction="row"
                  justifyContent="space-evenly"
                  alignItems="center"
                  spacing={2}
                  padding={1.5}
                  divider={<Divider orientation="vertical" flexItem />}
                >
                  <Typography alt={"Typ místnosti"}>
                    {item.type.name}
                  </Typography>
                  <Typography alt={"Cena"}>
                    {item.type.price + ",-Kč"}
                  </Typography>
                </Stack>
              </Card>
            </Grid>
          );
        }
      })}
    </Grid>
  );
}
