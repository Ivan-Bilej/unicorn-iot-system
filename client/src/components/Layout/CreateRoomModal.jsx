import React, { useState, useEffect } from "react";
import {Modal, Box, TextField, Select, MenuItem, Button, Stack} from "@mui/material";
import axiosInstance from "../../axios";

export default function CreateRoomModal({ open, onClose, roomTypes, editedRoom }) {
  const [formData, setFormData] = useState({
    // id: "",
    number: "",
    type: "",
  });

  // useEffect(() => {
  //     console.log("Data has changed")
  // },[formData])

  useEffect(() => {
    setFormData({
      // id: editedRoom?._id || "",
      number: editedRoom?.number || "",
      type: editedRoom?.type?._id || "",
    });
  }, [editedRoom]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async () => {
      console.log("formData:",formData)
    await axiosInstance.post(`/room/create`, formData).then((res) => {
        console.log(res);
    });
    onClose();
  };

  return (
      <Modal open={open} onClose={onClose}>
        <Box
            sx={{
              position: "absolute",
              width: 400,
              bgcolor: "background.paper",
              border: "2px solid #000",
              boxShadow: 24,
              p: 4,
              top: "50%",
              left: "50%",
              transform: "translate(-50%, -50%)",
            }}
        >
          <h2>{editedRoom ? "Edit Room" : "Add Room"}</h2>
          <Stack
              direction="column"
              justifyContent="flex-start"
              alignItems="flex-start"
              spacing={2}
          >
            <TextField
                fullWidth
                label="Room number"
                name="number" // Change this to "number"
                value={formData.number}
                onChange={handleChange}
            />
            <Select
                fullWidth
                label="Type"
                name="type"
                value={formData.type}
                onChange={handleChange}
            >
              {roomTypes.map((type) => (
                  <MenuItem key={type._id} value={type._id}>
                    {type.name}
                  </MenuItem>
              ))}
            </Select>
            <Stack
                direction="row"
                justifyContent="flex-start"
                alignItems="center"
                spacing={1}
            >
              <Button variant="contained" color="primary" onClick={onClose}>Cancel</Button>
              <Button variant="contained" color="success" onClick={handleSubmit}>
                {editedRoom ? "Save" : "Submit"}
              </Button>
            </Stack>
          </Stack>
        </Box>
      </Modal>
  );
}
