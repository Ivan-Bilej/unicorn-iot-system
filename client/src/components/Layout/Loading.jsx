import React from "react";
import { MagnifyingGlass } from "react-loader-spinner";

// Inline style for centering the spinner
const centerStyle = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "100vh", // This will take the full height of the viewport. Adjust as needed.
};

const Loading = ({ isLoading }) => {
  return isLoading ? (
    <div style={centerStyle}>
      <MagnifyingGlass
        visible={true}
        height="80"
        width="80"
        ariaLabel="magnifying-glass-loading"
        wrapperStyle={{}}
        wrapperClass="magnifying-glass-wrapper"
        glassColor="#c0efff"
        color="#e15b64"
      />
    </div>
  ) : null;
};

export default Loading;
