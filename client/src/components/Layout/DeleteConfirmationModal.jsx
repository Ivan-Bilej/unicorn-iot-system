import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {Button, Stack} from "@mui/material";
import axiosInstance from "../../axios";

const DeleteRoomModal = ({ open, handleClose, room, key, roomIndex , handleDelete, confirmationText}) => {
    // Access individual properties from the room object
    const { roomName} = room;

    // Rest of your component code
    // ...
    const handleModal = async () =>{
        console.log("room:",room._id)
        await axiosInstance.delete(`/room/${room._id}`).then((res) => {
            console.log(res)
        })
        handleDelete()
        handleClose()
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    return(
        <div>

            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Delete Room
                    </Typography>
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            {confirmationText} {roomName}?
                            <Stack
                                direction="row"
                                justifyContent="flex-start"
                                alignItems="center"
                                spacing={1}
                            >
                                <Button onClick={handleClose} variant="contained" color="info">Cancel</Button>
                                <Button onClick={handleModal} variant="contained" color="error">Delete</Button>
                            </Stack>
                    </Typography>
                </Box>
            </Modal>
        </div>
    )
};

export default DeleteRoomModal