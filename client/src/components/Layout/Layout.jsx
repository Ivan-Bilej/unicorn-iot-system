import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import HeroBannerImage from "../../images/layoutImage.jpg";
import Footer from "./Footer";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";
import Avatar from "@mui/material/Avatar";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { Button } from "@mui/material";
import axiosInstance from "../../axios";
import useUser from "../../hooks/useUser";
const settings = ["Logout"];

//TODO : Add function to logout from the account after onClick on Logout settings
const Layout = ({ children }) => {
  const user = useUser();
  const navigate = useNavigate();
  const [anchorElUser, setAnchorElUser] = useState(null);

  const handleReturnHome = () => {
    navigate("/");
  };
  const handleNavigateReservations = () => {
    navigate("/reservations");
  };

  const handleNavigateAdmin = () => {
    navigate("/admin");
  };
  const handleLogout = async () => {
    await axiosInstance.get("/logout");
    setAnchorElUser(null);
  };

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <div
      style={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}
    >
      <AppBar position="static">
        <Toolbar sx={{ justifyContent: "space-between" }}>
          <Typography
            variant="h6"
            component="div"
            onClick={handleReturnHome}
            sx={{
              flexGrow: 0,
              display: "flex",
              mr: 2,
            }}
          >
            Reservation App
          </Typography>

          <Box sx={{ flexGrow: 1, display: "flex" }}>
            <Button
              onClick={handleNavigateReservations}
              sx={{
                my: 1,
                color: "white",
                display: "block",
              }}
            >
              Reservations
            </Button>
          </Box>

          <Box
            sx={{
              display: "flex",
            }}
          >
            {user && user.data.role === "Admin" ? (
              <Button
                onClick={handleNavigateAdmin}
                sx={{
                  mr: 5,
                  color: "white",
                  display: "block",
                }}
              >
                Admin
              </Button>
            ) : null}
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar alt="User Avatar" src={user? user.data.image:null} />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: "45px" }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {settings.map((setting) => (
                <MenuItem
                  key={setting}
                  onClick={
                    setting === "Logout" ? handleLogout : handleCloseUserMenu
                  }
                >
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
        </Toolbar>
      </AppBar>

      <Container
        component="main"
        style={{ flex: 1 }}
        sx={{
          mt: 8,
          mb: 2,
        }}
      >
        <Box>{children}</Box>
      </Container>

      <Footer />
    </div>
  );
};

export default Layout;
