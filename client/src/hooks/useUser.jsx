// useAuth.js

import { useState, useEffect } from "react";
import axiosInstance from "../axios";

const useUser = () => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const getUser = async () => {
      try {
        const userData = await axiosInstance.get("/user");
        setUser(userData);
      } catch (error) {
        console.log(error);
      }
    };

    getUser();
  }, []);

  return user;
};

export default useUser;
