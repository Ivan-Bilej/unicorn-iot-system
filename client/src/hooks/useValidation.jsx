// useValidation.js
import { useState } from 'react';

export function useValidation() {
  const [errors, setErrors] = useState({});

  function validateField(fieldName, value) {
    const newErrors = { ...errors };

    // Check if Room number is not negative
    if (fieldName === "roomNumber") {
      if (value < 0 || String(value).includes("--")) {
        newErrors[fieldName] = "Room number must be positive number or Zero.";
      } else {
        delete newErrors[fieldName];
      }
    }

    // Check if Price is not negative
    if (fieldName === "price") {
      if (value < 0 || String(value).includes("--")) {
        newErrors[fieldName] = "Price must be positive number.";
      } else {
        delete newErrors[fieldName];
      }
    }

    // Check if Description is not longer than 4000 chars
    if (fieldName === "description") {
        if (value.length > 4000) {
            newErrors[fieldName] = "Description can contain at max 4000 characters"
        } else {
            delete newErrors[fieldName];
        }
    }

    // Check if every file is an image
    if (fieldName === "images") { 
      const allImages = Array.isArray(value) && value.every(file => file.type.startsWith('image/'));
      if (!allImages) {
        newErrors[fieldName] = "Only image files (like PNG, JPEG, etc.) are allowed.";
      } else {
        delete newErrors[fieldName];
      }
    }

    // Check if dateFrom is not older than todays date
    if (fieldName === "dateFrom") {
      // Format today's date as YYYY-MM-DD
      const today = new Date();
      const formattedToday = today.toISOString().split('T')[0];

      if (new Date(value) < new Date(formattedToday)) {
        newErrors[fieldName] = "Cannot make reservation older than todays date.";
      } else {
        delete newErrors[fieldName];
      }
    }

    setErrors(newErrors);
  }

  return { errors, validateField };
}
