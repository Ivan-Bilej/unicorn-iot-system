// useAuth.js

import { useState, useEffect } from "react";
import axiosInstance from "../axios";

const useAuth = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(null);

  useEffect(() => {
    const verifyUser = async () => {
      try {
        await axiosInstance.get("/user");
        setIsAuthenticated(true);
      } catch (error) {
        setIsAuthenticated(false);
      }
    };

    verifyUser();
  }, []);

  return isAuthenticated;
};

export default useAuth;
