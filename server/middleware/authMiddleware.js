function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next(); // The user is authenticated, so proceed with the next middleware or route handler
  } else {
    // The user is not authenticated, so redirect them to the login page or send an error response
    res.status(401).json({ message: "Unauthorized access." });
  }
}

module.exports = ensureAuthenticated;
