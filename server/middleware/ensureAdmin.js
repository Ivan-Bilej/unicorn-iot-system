function ensureAdmin (req, res, next) {
    if (req.isAuthenticated() && req.user.role === 'Admin') {
      return next();
    } else {
      return res.status(403).send('Access denied. Only admins are allowed.');
    }
  };
  
  module.exports = ensureAdmin;
  