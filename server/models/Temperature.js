const { Double, ObjectId } = require("mongodb");
const mongoose = require("mongoose");

const roomSchema = new mongoose.Schema({
  hardwareId: {
    type: Number,
    required: true,
    unique: true,
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId, // Reference to RoomType model
    ref: "User",
    required: true,
  },
  temperature: {
    type: Number,
    required: true
  },
  temperatureUnit: {
    type: String
  },
  _msgid: {
    type: ObjectId,
    required: true
  }
});

const Room = mongoose.model("Temperature", roomSchema);

module.exports = Room;
