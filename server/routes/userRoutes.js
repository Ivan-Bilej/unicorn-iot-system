const express = require("express");
const router = express.Router();
const ensureAuthenticated = require("../middleware/authMiddleware");
const User = require("../models/User");

// Route to get the current user
router.get("/", ensureAuthenticated, async (req, res) => {
  try {
    // The user ID is stored in req.session.passport.user when using Passport
    const user = await User.findById(req.session.passport.user);
    if (!user) {
      return res.status(404).send("User not found");
    }

    res.send(user);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get("/loginLink", async (req, res) => {
  const loginLink =
    process.env.ENVIROMENT === "PROD"
      ? "https://reservio-unic-cefb78c42714.herokuapp.com/auth/google"
      : "http://localhost:3000/auth/google";
  res
    .send({
      loginLink,
    })
    .status(200);
});

module.exports = router;
