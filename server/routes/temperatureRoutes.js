const express = require("express");
const router = express.Router();
const temperatureService = require("../services/temperature.service.js");

router.post("/write", async (req, res) => {
  try {
    const temperatureData = req.body;
    const temperature = await temperatureService.writeTemperature(temperatureData);
    res.status(201).send(temperature);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const temperatureId = req.params.id;
    const result = await temperatureService.deleteTemperature(temperatureId);
    if (result.deletedCount === 0) {
      return res.status(404).send("Temperature not found");
    }
    res.send("Temperature successfully deleted");
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get("/list", async (req, res) => {
  try {
    const temperatures = await temperatureService.listAllTemperatures();
    res.send(temperatures);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.get("/list/:iotId", async (req, res) => {
    try {
      const iotId = req.params.id;
      const temperatures = await temperatureService.listTemperatureByIot(iotId);
      res.send(temperatures);
    } catch (error) {
      res.status(500).send(error.message);
    }
  });

router.get("/:id", async (req, res) => {
  try {
    const temperatureId = req.params.id;
    const temperatures = await temperatureService.findTemperature(temperatureId);
    res.send(temperatures);
  } catch (error) {
    res.status(500).send(error.message);
  }
});

router.put("/:id", async (req, res) => {
  try {
    const temperatureId = req.params.id;
    const updateData = req.body;

    const updatedTemperature = await temperatureService.updateTemperature(temperatureId, updateData);

    res.send(updatedTemperature); // Send the updated temperature as a response
  } catch (error) {
    res.status(500).send(error.message);
  }
});

module.exports = router;
