//načtení modulu express
const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const passport = require("passport");
const session = require("cookie-session");

const userRoutes = require("./routes/userRoutes");
const temperatureRoutes = require("./routes/temperatureRoutes");

const authMiddleware = require("./middleware/authMiddleware");

const path = require("path");

require("dotenv").config({ path: __dirname + "/.env" });
require("./config/passport")(passport);

const mongoDB = process.env.MONGODB;
mongoose
  .connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("MongoDB Connected"))
  .catch((err) => console.log("MongoDB connection error:", err));

const app = express();
const port = process.env.PORT || 3000;

// Parsování body
app.use(express.json()); // podpora pro application/json
app.use(express.urlencoded({ extended: true })); // podpora pro application/x-www-form-urlencoded

if (process.env.ENVIRONMENT === "DEV") {
  app.use(cors({ credentials: true, origin: "http://localhost:5173" }));
}

if (process.env.ENVIRONMENT === "PROD") {
  app.use(
    cors({
      credentials: true,
      origin: "https://reservio-unic-cefb78c42714.herokuapp.com/",
    })
  );
  app.set("trust proxy", 1);
}

app.use(session({ secret: "SECRET", resave: false, saveUninitialized: true }));

app.use(passport.initialize());
app.use(passport.session());

app.get(
  "/auth/google",
  passport.authenticate("google", { scope: ["profile", "email"] })
);
app.get(
  "/auth/google/callback",
  passport.authenticate("google", { failureRedirect: "/login" }),
  function (req, res) {
    res.redirect("/");
  }
);

app.get("/logout", (req, res) => {
  req.logout(); // Passport provides this function on the request object
  res.redirect("/"); // Redirect to the home page after logging out
});

app.use("/api/user", userRoutes);


function applyAuthMiddleware(req, res, next) {
  if (req.path.startsWith("/api")) {
    authMiddleware(req, res, next); // Apply authMiddleware only for routes starting with /api
  } else {
    next(); // For non-API routes, continue to the next middleware or route handler
  }
}

app.use(applyAuthMiddleware);


// Protected routes

app.use("/api/temperatures", temperatureRoutes);

// Serve static files from the React app
app.use(express.static(path.join(__dirname, "../client/dist")));

// Catchall handler for frontend routes
app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname, "../client/dist/index.html"));
});

//nastavení portu, na kterém má běžet HTTP server
app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});
