const Temperature = require("../models/Temperature");

const temperatureService = {
  async writeTemperature(iotData) {
    try {
      const temperature = new Temperature(iotData);
      await temperature.save();
      return temperature;
    } catch (error) {
      if (error.code === 11000) {
        throw new Error("Temperature data already exists.");
      } else {
        throw new Error("Error saving temperature data: " + error.message);
      }
    }
  },

  async deleteTemperature(temperatureId) {
    try {
      const result = await Temperature.deleteOne({ _id: temperatureId });
      if (result.deletedCount === 0) {
        throw new Error("Temperature data not found.");
      }
      return result;
    } catch (error) {
      throw new Error("Error deleting temperature: " + error.message);
    }
  },

  async listAllTemperatures() {
    try {
      return await Temperature.find({});
    } catch (error) {
      throw new Error("Error fetching temperatures data: " + error.message);
    }
  },

  async listTemperatureByIot(iotId) {
    try {
      return await Temperature.find({iotId: iotId});
    } catch (error) {
      throw new Error("Error fetching temperatures data for IoT: " + error.message);
    }
  },

  async findTemperature(temperatureId) {
    try {
      return await Temperature.findOne({ _id: temperatureId });
    } catch (error) {
      throw new Error("Error fetching temperature: " + error.message);
    }
  },

  async updateTemperature(temperatureIdId, updateData) {
    try {
      const updatedTemperature = await Temperature.findByIdAndUpdate(temperatureId, updateData, {
        new: true, // Return the updated temperature
        runValidators: true, // Run validation checks
      });

      if (!updatedTemperature) {
        throw new Error("Temperature not found.");
      }

      return updatedTemperature;
    } catch (error) {
      throw new Error("Error updating temperature: " + error.message);
    }
  },
};

module.exports = temperatureService;
